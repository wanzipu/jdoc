# jfinal企业文档管理系统

#### 介绍
借鉴OpenKM的基本功能，对文件、网页文章、脑图等知识进行统一管理，采用树状结构便于浏览，基于角色和用户的权限管理系统，可以对知识的阅读、下载、授权等操作进行细粒度授权，系统采用java elementui 技术uniapp制作手机端可以支持多端应用

#### 软件架构

-  Jfinal
-  element ui
-  mysql




#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

一、功能点设计
系统采用java elementui 技术uniapp制作手机端可以支持多端应用

1、树状结构

设置左边属性结构同windows 一样的操作习惯降低学习成本，支持基本的右键操作

![输入图片说明](https://img-blog.csdnimg.cn/20201218152241904.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")

2、权限设置

对节点实施基于角色和用户的权限管理，方便企业文档保密

![输入图片说明](https://img-blog.csdnimg.cn/20201218152521627.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")

3、文件预览（pdf）

对office文件 word excel ppt 图片 文本文件 上传后，开启线程 自动生成pdf文件，方便预览。
![输入图片说明](https://img-blog.csdnimg.cn/20201218152745470.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")


4、文件上传

自动检查文件类型，记录父子关系

5、创建文章、脑图

节点不仅仅以文件的形式，还以脑图和文章的形式存在，直接网页访问
![输入图片说明](https://img-blog.csdnimg.cn/20201218152947696.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")

![输入图片说明](https://img-blog.csdnimg.cn/2020121815305396.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")


5、批量上传

通过zip文件，自动上传多个文件，并自动解压，支持多级文件夹自动解压成文件节点

6、节点复制与移动

文件节点移动和复制 实现深层拷贝

7、全文索引（lucene）

上传后自动对标题和内容进行全文索引，程序自动提取word excel pdf ppt 等文本 进行索引

8、系统管理配置

系统管理部分的设计
![输入图片说明](https://img-blog.csdnimg.cn/20201218153343142.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")


9、手机端

手机端主要设置预览和检索功能

 
![输入图片说明](https://images.gitee.com/uploads/images/2020/1221/162823_2ea84511_21282.png "屏幕截图.png")

![输入图片说明](https://img-blog.csdnimg.cn/20201218154043468.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3lhbnNoYW96aGk=,size_16,color_FFFFFF,t_70 "在这里输入图片标题")
 


todo：

         bug修复 细节完善

         回收站功能、归档功能、

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
